---
title: Whiskey Sour
authors: cocktailian
tags:
- Whiskey
- Rye
- Whiskey Sour
- Sour
---


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | Rye Whiskey |
|     3 | cl        | Zitronensaft |
|     2 | cl        | Zuckersirup |
|   1-2 | Dashes    | Angostura Bitters |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.

