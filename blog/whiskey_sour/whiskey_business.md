---
title: Whiskey Business
authors: cocktailian
tags:
- Whiskey
- Rye
- Whiskey Sour
- Sour
- Whiskey Business
---


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | Rye Whiskey |
|     3 | cl        | [Ancho Reyes](https://www.drinks.ch/de/ancho-reyes-chile-liqueur-70cl.html) |
|   1,5 | cl        | Zitronensaft |
|   1,5 | cl        | Zimmtsirup |
|   1-2 | Dashes    | Angostura Bitters |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
