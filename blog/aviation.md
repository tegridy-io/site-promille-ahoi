---
title: Aviation
authors: cocktailian
tags:
- Gin
- Aviation
- Crème de Violette
---

Der Aviation ist ein weiterer vergessener Klassiker, den Bartender in den letzten Jahren wieder aus der Versenkung holten.
Uraufgeführt wurde der Aviation in Hugo Ensslins Buch _"Recipes for Mixed Drinks"_ im Jahre 1916, dor jedoch unter Zuhilfenahme einer weiteren Zutat: Crème de Violette, ein französischer Veilchenlikör.
Dadurch macht der Name des Cocktails nun auch Sinn, denn dieser Hauch von Blau, erinnert in der Tat an den Himmel und die Luftfahrt.
Der Aviation ist ein leichter und floraler Drink, bei dem sehr schnell das Süsse / Säure Verhältnis kippt.
Sollte nicht der bevorzugte relativ trockene Luxardo Maraschino verwendet werden, sollte die Rezeptur eventuell and das Mehr an Süsse eines anderen Maraschinolikörs anpassen.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | Gin |
|     1 | cl        | [Luxardo Maraschino](https://www.drinks.ch/de/luxardo-marascino-liqueur-50cl.html) |
|     1 | cl        | [Crème de Violette](https://www.galaxus.ch/en/s7/product/the-bitter-truth-tbt-creme-de-violete-50-cl-liqueur-2739450) |
|   2,5 | cl        | Zitronensaft |
|     1 | BL        | Zuckersirup |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
