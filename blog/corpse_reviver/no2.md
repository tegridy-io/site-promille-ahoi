---
title: Corpse Reviver No. 2
authors: cocktailian
tags:
- Gin
- Corpse Reviver
- Signature Drink
---

Wie der Name verrät, ist dieser Drink als _Pick me up_ gedacht.
Er soll den Körper nach einer feucht-fröhlichen Nacht wiederbeleben, was jedoch bei fast sieben cl Alkohol auch nach hinten losgehen kann.


| Zutaten |||
| ----: | :-------- | :-------- |
|     2 | cl        | Gin |
|     2 | cl        | [Dwersteg Orangen Likör](https://www.galaxus.ch/en/s7/product/dwersteg-organic-orange-liqueur-50-cl-liqueur-5991974) |
|     2 | cl        | Zitronensaft |
|     2 | cl        | [Lillet Blanc](https://www.drinks.ch/de/lillet-blanc-aperitif-75cl.html) |
|     1 | BL        | Absinth |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Glas mit Absinth benetzen.
Doppelt in das vorgekühlte Glass abseihen.
