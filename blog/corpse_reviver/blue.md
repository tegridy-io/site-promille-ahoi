---
title: Corpse Reviver No. Blue
authors: debakelorakel
tags:
- Gin
- Corpse Reviver
---

Eine klein Abwandlung des No. 2, bei dem lediglich der Triple sec mit Blue Curaçao getauscht wird.

| Zutaten |||
| ----: | :-------- | :-------- |
|     2 | cl        | Gin |
|     2 | cl        | [Blue Curaçao](https://www.drinks.ch/de/de-kuyper-blue-curacao-likoer-70cl.html) |
|     2 | cl        | Zitronensaft |
|     2 | cl        | [Lillet Blanc](https://www.drinks.ch/de/lillet-blanc-aperitif-75cl.html) |
|     1 | BL        | Absinth |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Glas mit Absinth benetzen.
Doppelt in das vorgekühlte Glass abseihen.

