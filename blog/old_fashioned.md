---
title: Old Fashioned
authors: cocktailian
tags:
- Whiskey
- Bourbon
- Old Fashioned
- Signature Drink
---

Schon lange vor der ersten Folge Mad Men galt der Old Fashioned als Urgestein unter den Cocktails.
Um die 1860er Jahre tauchten erste Rezepte, noch unter dem Namen Whiskey Cocktail, in verschiedenen Barbüchern auf.
Zu Zeiten der Prohibition erfreute sich der Drink besonderer Beliebtheit – der Zucker machte den schwarz gebrannten Whiskey etwas genießbarer.
Heute hat der Klassiker aus Whiskey, Zucker und Angostura nicht nur ein eigenes Glas, sondern existiert dazu in gelungenen Varianten mit Brandy bis Mezcal.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | [Wild Turkey Rare Breed](https://www.drinks.ch/de/wild-turkey-rare-breed-barrel-proof-116-8-bourbon-whiskey-70cl.html) |
|     1 | BL        | Zuckersirup |
|   5-6 | Dashes    | Angostura Bitters |


Alle Zutaten mit Eis in ein Rührglass geben und solange verrühren, bis das Glas beschlägt.
In ein Glas mit einem grossen Eiswürfel abseihen.
