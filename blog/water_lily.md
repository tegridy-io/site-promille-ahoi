---
title: Water Lily
authors: trinkabenteuer
tags:
- Gin
- Water Lily
- Crème de Violette
- Signature Drink
---

Der Water Lily Cocktail ist einer der wenigen Drinks, in denen der Veilchenlikör Crème de Violette nicht nur Nebenspieler ist, sondern zu einem der Hauptakteure wird.
Das funktioniert erstaunlich gut – und blau-violett ist der Drink auch noch!

Auch die Stabilität ist erstaunlich am Water Lily Cocktail.
Drinks kippen mit der Zeit und mit zunehmender Wärme in eine zu süße Richtung, aber die Süße nimmt hier nicht so rasch Überhand, wie man es bei der Kombination von zwei Likören dieser Art vermuten könnte.
Der Triple Sec zeigt sich vielmehr als hervorragender Partner zum Veilchenlikör und lässt sich in seiner Robustheit nicht so leicht die aromatische Butter vom Brot nehmen.
Dass man sich bei zwei so prägnanten Aromagebern bei der Wahl des Gins auf einen klassischen, knackigen London Dry beschränken sollte, versteht sich von selbst.


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | Gin |
|     3 | cl        | [Dwersteg Orangen Likör](https://www.galaxus.ch/en/s7/product/dwersteg-organic-orange-liqueur-50-cl-liqueur-5991974) |
|     3 | cl        | [Crème de Violette](https://www.galaxus.ch/en/s7/product/the-bitter-truth-tbt-creme-de-violete-50-cl-liqueur-2739450) |
|     3 | cl        | Zitronensaft |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
