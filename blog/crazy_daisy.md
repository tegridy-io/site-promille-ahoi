---
title: Crazy Daisy
authors: trinkabenteuer
tags:
- Whiskey
- Scotch
- Crazy Daisy
- Signature Drink
---


| Zutaten |||
| ----: | :-------- | :-------- |
|     5 | cl        | [Fettercairn Highland Single Malt](https://www.drinks.ch/de/fettercairn-highland-single-malt-12-years-70cl.html) |
|     2 | cl        | [Dwersteg Orangen Likör](https://www.galaxus.ch/en/s7/product/dwersteg-organic-orange-liqueur-50-cl-liqueur-5991974) |
|     2 | cl        | [Meneau Orgeat](https://www.green-shop.ch/fr/meneau/7793-sirop-d-orgeat-bio-50cl-meneau-3274490970298.html) |
|     3 | cl        | Zitronensaft |
|   3-4 | cl        | Sodawasser |
|     2 | Dashes    | Aromatic Bitters |
|     1 | cl        | Peaty Scotch (Optional) |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
Mit Sodawasser auffüllen, optional mit peaty Scotch floaten.


Optionen:

[Glenmorangie 10 Years](https://www.drinks.ch/de/glenmorangie-10-years-single-malt-whisky-70cl.html)
