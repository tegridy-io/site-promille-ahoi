---
title: Hemingway Daiquiri
authors: cocktailian
tags:
- Rum
- Daiquiry
---

Eine der diversen Daiquiri-Ableitungen, die in der Floridita Bar entstanden sind.
Die Kombination von Maraschino und Pink Grapefruitsaft ist unschlagbar, aber achten sie beim Kirschlikör auf Qualität.
Herb, frisch und floral erfrischt dieser Cocktail ihren Gaumen, salud!


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | [Havanna Club](https://www.drinks.ch/de/havana-club-3-anos-rum-100cl.html) |
|     2 | cl        | Limettensaft |
|     1 | cl        | [Luxardo Maraschino](https://www.drinks.ch/de/luxardo-marascino-liqueur-50cl.html) |
|     1 | cl        | Zuckersirup |
|     3 | cl        | Pink Grapefruitsaft |
|   1-2 | Dashes    | Angostura Bitters |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
