---
title: Classic Daiquiri
authors: cocktailian
tags:
- Rum
- Daiquiry
- Sour
---

Ob dieser Drink wirklich, wie man oft hört, von zwei amerikanischen Mineningenieuren in der kleinen kubanischen Stadt Daiquiri um 1900 erfunden wurde, ist eher anzuzweifeln.
Dafür ist die Kombination aus Limetten un Rum zu diesem Zeitpunkt schon zu weit verbreitet gewesen.
Zu Weltruhm gelangte der Daiquiri jedenfalls durch die Floridita Bar in Havana.
Constante Ribalaigua servierte ihn dort nicht nur Ernest Hemingway (in der Spezialversion als _Papa Doble_ mit doppelter Menge Rum und ohne Zucker), sondern während der Prohibition auch Heerscharen anderer Amerikaner.
Mr. Waring und seine Erfindung des elektrischen Blenders sorgte für den Rest der Erfolgsstory.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | Rum |
|     3 | cl        | Limettensaft |
|     2 | cl        | Zuckersirup |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
