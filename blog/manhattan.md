---
title: Manhattan
authors: cocktailian
tags:
- Whiskey
- Bourbon
- Rye
- Manhattan
---

Der Zeitpunkt, zu dem Bartender den Wermut für sich entdeckten, könnte man getrost die Geburtsstunde des _Manhattan_ und _Martini_ nennen.
Mit dem Wermut eröffnete sich ein ganz neues Betätigungsfeld für die Mixologen des 19. Jahrhunderts und sie nutzen es auf vielfältigste Art und Weise.
Man kann behaupten, dass Spirituosen und Wermut füreinander geschaffen sind.
Die unzähligen Male, die sie in Cocktails erfolgreich vermählt wurden, sind der Beweis dafür.

Um die Entstehung des Manhattan ranken sich selbstredend einige MYthen, ziemlich sicher ist aber, dass er in New York zwischen 1870 und 1884 entstanden ist.
Die oft verbreitete Theorie, dass der Manhattan Cocktail anlässlich eines Banketts von Jennie Jerome im New Yorker Manhattan Club kreiert wurde, ist mittlerweile widerlegt.
Mrs. Jerome befand sich zur besagten Zeit nämlich in England, wo sie ihren Sohn Winston Churchill gebar.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | Bourbon oder Rye Whiskey |
|     3 | cl        | Roter Vermouth |
|     2 | Dashes    | Angostura Bitters |

Zutaten mit Eis in einem Rührglass für 30 bis 40 Sekunden rühren.
In ein gekühltes Cocktail-Glass abseihen, mit einer Maraschino Kirsche garnieren.
