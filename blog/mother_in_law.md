---
title: Mother in Law
authors: cocktailian
tags:
- Whiskey
- Bourbon
- Mother in Law
---

Dieses Rezept wurde in der Schublade einer älteren Dame gefunden, die 1895 in New Orleans geboren, dieses Rezept von ihrer Schwiegermutter vermacht bekam.
Ted Haigh, Autor des Buches _"Vintage Spirits & Forgotten Cocktails"_ war der erste, der dieses Rezept wieder der Öffentlichkeit zugänglich machte.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | [Wild Turkey Rare Breed](https://www.drinks.ch/de/wild-turkey-rare-breed-barrel-proof-116-8-bourbon-whiskey-70cl.html) |
|     1 | BL        | [Dwersteg Orangen Likör](https://www.galaxus.ch/en/s7/product/dwersteg-organic-orange-liqueur-50-cl-liqueur-5991974) |
|     1 | BL        | [Luxardo Maraschino](https://www.drinks.ch/de/luxardo-marascino-liqueur-50cl.html) |
|     1 | BL        | Zuckersirup |
|   2-4 | Dashes    | Peychaud Bitters |
|   2-4 | Dashes    | Angostura Bitters |
|   2-4 | Dashes    | Amer Picon |


Alle Zutaten mit Eis in ein Rührglass geben und solange verrühren, bis das Glas beschlägt.
In vorgekühltes Glas abseihen.
