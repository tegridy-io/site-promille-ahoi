---
title: Negroni
authors: debakelorakel
tags:
- Gin
- Negroni
- Signature Drink
---

Eine leichte Abwandlung des klassischen Negroni, legt mehr Betonung auf den Gin.


| Zutaten |||
| ----: | :-------- | :-------- |
|     4 | cl        | Gin |
|     3 | cl        | [Noilly Prat Amber](https://www.galaxus.ch/de/s7/product/noilly-prat-vermouth-ambre-16-frankreich-bitter-aperitif-21590028) |
|     2 | cl        | [Campari](https://www.drinks.ch/de/campari-bitter-100cl.html) |
|   1-2 | Dash      | Peychaud Bitters |


Alle Zutaten mit Eis in ein Rührglass geben und solange verrühren, bis das Glas beschlägt.
In vorgekühltes Glas abseihen.
