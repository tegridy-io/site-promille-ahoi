---
title: Classic Negroni
authors: cocktailian
tags:
- Gin
- Negroni
---

Der Negroni Cocktail ist und bleibt einer der klassischen Aperitif-Cocktails der heutigen Zeit. Der Drink entstammt natürlich dem Land, dem wir die Aperitif-Kultur überhaupt zu verdanken haben: Italien.

Die Erfindung des Negroni war im Übrigen wohl mehr Zufall als Planung. Fosco Scarselli, Bartender im Caffé Casoni in Florenz versorgte den Grafen Camillo Negroni um 1920 mit seinem üblichen Labsal, dem Americano. Eines Tages dürstete es dem Grafen nach etwas Stärkerem und er verlangte von Scarselli anstelle des Sodawassers einen Schuss Gin in seinem Americano, schnell wurde der Americano nach Negroni-Art in Florenz populär und schon bald war er fester Bestandteil der italienischen Aperitif-Kultur.


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | Gin |
|     3 | cl        | Roter Vermouth |
|     3 | cl        | [Campari](https://www.drinks.ch/de/campari-bitter-100cl.html) |
|   1-2 | Dash      | Orange Bitters |


Alle Zutaten mit Eis in ein Rührglass geben und solange verrühren, bis das Glas beschlägt.
In vorgekühltes Glas abseihen.
