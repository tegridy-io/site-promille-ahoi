---
title: Boulvardier
authors: cocktailian
tags:
- Whiskey
- Bourbon
- Boulvardier
- Negroni
---

Eine weitere Variante des Negroni, jedoch völlig unabhängig von diesem entstanden.
Wie auch beim Agavoni wurde die basisspirituose ersetzt, die Rezeptur aber bei gleichem Verhältnis der Zutaten zueinander belassen.
Mit diesem ausdrucksvollen Bourbon über 45% Vol. ein wunderbar ausgewogener Drink.


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | [Wild Turkey Rare Breed](https://www.drinks.ch/de/wild-turkey-rare-breed-barrel-proof-116-8-bourbon-whiskey-70cl.html) |
|     3 | cl        | Roter Vermouth |
|     3 | cl        | [Campari](https://www.drinks.ch/de/campari-bitter-100cl.html) |
|   1-2 | Dash      | Orange Bitters |


Alle Zutaten mit Eis in ein Rührglass geben und solange verrühren, bis das Glas beschlägt.
In vorgekühltes Glas abseihen.
