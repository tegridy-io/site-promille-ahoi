---
title: El Negroni de Juárez
authors:
  name: Hotel Vilòn, Rome
tags:
- Mezcal
- Negroni
- Signature Drink
---

Eine kreative Abwandlung mit Mezcal


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | Mezcal |
|     3 | cl        | [Cocchi Storico](https://www.galaxus.ch/de/s7/product/cocchi-storico-vermouth-di-torino-18-jahre-16-italien-1-x-75-cl-wermut-aperitif-10273222?ip=cocchi) |
|     3 | cl        | [Rinomato Bitter](https://www.urban-drinks.de/rinomato-bitter-scuro-10l-23-vol.html) |
|     2 | Dash      | [Fernet Branca](https://www.drinks.ch/de/fernet-branca-bitter-10cl.html) |


Alle Zutaten mit Eis in ein Rührglass geben und solange verrühren, bis das Glas beschlägt.
In vorgekühltes Glas abseihen.
