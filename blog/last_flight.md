---
title: Last Flight
authors: debakelorakel
tags:
- Whiskey
- Bourbon
- Last Flight
- Chartreuse
---

Der Last Flight Cocktail ist ein süss / saurer Bourbon Cocktail und ist eine Mischung aus dem Paper Plane und dem Last Word.
Hergestellt mit gleichen Teilen von vier Bar-Klassikern.


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | [Wild Turkey Rare Breed](https://www.drinks.ch/de/wild-turkey-rare-breed-barrel-proof-116-8-bourbon-whiskey-70cl.html) |
|     3 | cl        | [Chartreuse Verte](https://www.drinks.ch/de/chartreuse-verte-liqueur-70cl.html) |
|     3 | cl        | [Campari](https://www.drinks.ch/de/campari-bitter-100cl.html) |
|     3 | cl        | Zitronensaft |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
