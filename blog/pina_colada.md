---
title: Cristal Clear Piña Colada
authors: trinkabenteuer
tags:
- Rum
- Coconut
- Pina Colada
---

Die kristallklare Piña Colada als Highball Version ist eine erfrischend , überraschende Version des großen karibischen Rum Klassiker aus Ananas und Kokosnuss.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | [Aluna Coconut Rum](https://www.masterofmalt.com/rum/aluna/aluna-coconut-rum/) |
|     3 | cl        | [De Kuyper Ananas Likör](https://www.drinks.ch/de/de-kuyper-pineapple-likor-70cl.html) |
|     1 | cl        | [Supasawa](https://www.drinks.ch/de/supasawa-cocktail-mixer-70cl.html) |
|   5-6 | cl        | Sodawasser |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
Mit Sodawasser auffüllen.
