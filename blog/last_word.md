---
title: Last Word
authors: cocktailian
tags:
- Gin
- Last Word
- Chartreuse
- Signature Drink
---

Wow! Diese Aussage hört man oft, wenn man diesen Drink einem Gast zum ersten mal kredenzt.
Auf den ersten Blick scheinen die Liköre überdosiert und die gleichen Teile aller Zutaten verunsichern so manchen Bartender, aber:
Dieser Drink ist perfekt ausbalanciert, sofern man einen kräftigen klassischen Gin verwendet!


| Zutaten |||
| ----: | :-------- | :-------- |
|     3 | cl        | Gin |
|     3 | cl        | [Chartreuse Verte](https://www.drinks.ch/de/chartreuse-verte-liqueur-70cl.html) |
|     3 | cl        | [Luxardo Maraschino](https://www.drinks.ch/de/luxardo-marascino-liqueur-50cl.html) |
|     3 | cl        | Limettensaft |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
