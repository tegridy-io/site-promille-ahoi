---
title: Ryan Air
authors: debakelorakel
tags:
- Gin
- Aviation
- Ryan Air
- Crème de Violette
---

Eine weitere Abwandlung eines köstlichen Klassikers, der zwar viel verspricht aber nicht alles hält.
Manchmal bringt man halt nicht alles mit was für einen ausgewogenen Drink von nöten ist.
Oder das production Keycloak schmiert gerade ab und es muss schnell gehen.


| Zutaten |||
| ----: | :-------- | :-------- |
|     6 | cl        | Gin |
|     1 | cl        | [Dwersteg Orangen Likör](https://www.galaxus.ch/en/s7/product/dwersteg-organic-orange-liqueur-50-cl-liqueur-5991974) |
|     1 | cl        | [Crème de Violette](https://www.galaxus.ch/en/s7/product/the-bitter-truth-tbt-creme-de-violete-50-cl-liqueur-2739450) |
|     3 | cl        | Zitronensaft |
|     1 | BL        | Orgeat |


Alle Zutaten in einen Shaker geben, diesen mit Eis füllen und kräftig 10 bis 15 Sekunden schütteln.
Doppelt in das vorgekühlte Glass abseihen.
