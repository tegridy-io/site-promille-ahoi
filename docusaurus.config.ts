import {themes as prismThemes} from 'prism-react-renderer';
import type {Config} from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

const config: Config = {
  title: 'Promille Ahoi',
  tagline: 'Bibo Ergo Sum',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://promille.ahoi.sh',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'tegridy.io',
  projectName: 'site-promille-ahoi',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'de',
    locales: ['de'],
  },

  presets: [
    [
      'classic',
      {
        blog: {
          showReadingTime: false,
          blogSidebarCount:	0,
          blogSidebarTitle: 'Alle Cocktails',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/-/ide/project/tegridy.io/site-promille-ahoi/tree/master/-/',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      } satisfies Preset.Options,
    ],
  ],

  themeConfig: {
    // Replace with your project's social card
    image: 'img/cocktails.png',
    navbar: {
      title: 'Promille Ahoi',
      logo: {
        alt: 'Site Logo',
        src: 'img/logo.svg',
        srcDark: 'img/logo_dark.svg',
      },
      items: [
        {to: '/blog/tags/signature-drink', label: 'Signature Drinks', position: 'left'},
        {to: '/blog/tags/gin', label: 'Gin', position: 'left'},
        {to: '/blog/tags/rum', label: 'Rum', position: 'left'},
        // {to: '/blog/tags/tequila', label: 'Tequila', position: 'left'},
        {to: '/blog/tags/whiskey', label: 'Whiskey', position: 'left'},
        {to: '/blog/tags', label: 'Index', position: 'right'},
      ],
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} tegridy.io, built while drunk.`,
    },
    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
    },
  } satisfies Preset.ThemeConfig,
};

export default config;
