FROM node:20-alpine

EXPOSE 3000/tcp

RUN apk add git && apk cache clean

USER 1000:0

VOLUME /data
WORKDIR /data

CMD yarn install; yarn start --host 0.0.0.0
